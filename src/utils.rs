#[cfg(test)]
use std::sync::atomic::{AtomicUsize, Ordering};
#[cfg(test)]
use postgres::{Connection, TlsMode};

#[cfg(test)]
pub fn get_connection() -> Connection {
    static COUNTER: AtomicUsize = AtomicUsize::new(0);
    let conn = Connection::connect("postgresql://flit:flit@postgres/flit", TlsMode::None)
        .unwrap();
    let new_db = format!("flit{}", COUNTER.fetch_add(1, Ordering::Relaxed));
    conn.batch_execute(&format!("CREATE DATABASE {} OWNER flit;", new_db))
        .unwrap();

    let url = format!("postgresql://flit:flit@postgres/{}", new_db);
    Connection::connect(url, TlsMode::None).unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_check_connection() {
        only_ci!({
            let conn = get_connection();
            assert!(conn.is_active());
        });
    }
}
