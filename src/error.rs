error_chain! {
    foreign_links {
        Regex(::regex::Error);
        ParseInt(::std::num::ParseIntError);
        Io(::std::io::Error);
        Postgres(::postgres::Error);
    }

    errors {
        NotADirectory {
            description("only folder can be migrations")
        }

        InvalidPathName {
            description("attempted to parse a path name and failed")
            display("couldn't parse due to invalid path name")
        }

        NoUpSqlFileFound(detail: ::std::path::PathBuf) {
            description("couldn't find an up.sql file in the current migration")
            display("couldn't find an up.sql file in the current migration {:?}", detail)
        }

        NoSuchMigration(id: i64) {
            description("tried to roll back to a migration with no such ID")
            display("no migration with ID {} found", id)
        }

        NoDownScript(id: i64) {
            description("the migration didn't have a Some value for sql_down")
            display("no down script available for {}", id)
        }

        Irreversible {
            description("A migration wasn't reversible")
            display("a migration wasn't reversible (no down.sql) in the chain to reverse a migration")
        }
    }
}
