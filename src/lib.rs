extern crate chrono;
extern crate postgres;
extern crate regex;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate log;

macro_rules! only_ci {
    ($e:expr) => {
        match ::std::env::var("CI") {
            Ok(ref ci) if ci != "" => {
                $e;
                ()
            }
            Ok(_) | Err(_) => (),
        };
    };
}

mod error;
mod migration;
mod utils;

pub use error::Error;
pub use migration::{Migration, MigrationManager};

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
