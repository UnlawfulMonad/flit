use error as e;
use postgres::{self, Connection};
use regex::Regex;
use std::collections::HashMap;
use std::ffi;
use std::fs;
use std::path::Path;

/** A single migration and (optionally) how to undo it.
 */
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Migration {
    pub id: i64,
    /// The name of the migration
    pub name: String,

    /// Whether to run this migration as a part of a transaction or not.
    pub transaction: bool,

    /// The raw SQL.
    pub sql_up: String,
    pub sql_down: Option<String>,
}

impl Migration {
    /** Load a migration given a path. The path *must* be formatted in the form `<id>_<name>`
     * where `id` is a number used for ordering migrations (usually this'd be a timestamp of the
     * form `201809251700`) and name is any valid path name.
     */
    pub fn load_migration<P: AsRef<Path>>(path: P) -> e::Result<Migration> {
        let meta = fs::metadata(path.as_ref())?;
        if !meta.is_dir() {
            bail!(e::ErrorKind::NotADirectory);
        }

        lazy_static! {
            static ref regexp: Regex = Regex::new(r"^(\d+)_(.+)$").unwrap();
        }

        let path_str: String =
            path.as_ref().file_name().unwrap().to_string_lossy().into();
        let captures = match regexp.captures(&path_str) {
            Some(path) => path,
            None => bail!(e::ErrorKind::InvalidPathName),
        };

        let id = captures[1].parse()?;
        let name: String = captures[2].into();
        assert!(id >= 0);
        assert!(name.len() > 0);

        let files =
            fs::read_dir(path.as_ref())?.filter_map(|entry| match entry {
                Ok(readdir) => Some(readdir.path()),
                Err(_) => None,
            });

        let mut sql_up = None;
        let mut sql_down = None;
        let mut transaction = true;

        for file in files {
            let migration_file_name = if let Some(name) =
                file.file_name().and_then(|n| (n as &ffi::OsStr).to_str())
            {
                name.to_string()
            } else {
                continue;
            };

            match &migration_file_name as &str {
                "up.sql" => {
                    sql_up = Some(fs::read_to_string(&file)?);
                }
                "down.sql" => {
                    sql_down = Some(fs::read_to_string(&file)?);
                }
                "no_transaction" => {
                    transaction = false;
                }
                _ => (),
            }
        }

        let sql_up = if sql_up.is_some() {
            sql_up.unwrap()
        } else {
            bail!(e::ErrorKind::NoUpSqlFileFound(path.as_ref().into()));
        };
        Ok(Migration {
            id,
            name,
            sql_up,
            sql_down,
            transaction,
        })
    }

    pub fn is_reversible(&self) -> bool {
        self.sql_down.is_some()
    }
}

#[derive(Debug, Clone)]
pub struct MigrationManager {
    migrations: HashMap<i64, Migration>,
}

impl MigrationManager {
    /**
     * Create a new `MigrationManager`.
     *
     * # Example
     * ```
     * # use flit::MigrationManager;
     * let migration_manager = MigrationManager::new();
     */
    #[must_use]
    pub fn new() -> MigrationManager {
        MigrationManager {
            migrations: HashMap::new(),
        }
    }

    /**
     * Add a new migration.
     *
     * # Returns
     * `None` if the migration was added successfully. `Some(migration)` otherwise.
     *
     * # Examples
     * ```
     * # use flit::{Migration, MigrationManager};
     * # let mut mm = MigrationManager::new();
     * let migration1 = Migration {
     *     id: 0,
     *     name: "01_init".into(),
     *     transaction: true,
     *     sql_up: "".into(),
     *     sql_down: None,
     * };
     * let migration2 = migration1.clone();
     *
     * // Haven't added yet
     * assert!(mm.add_migration(migration1).is_none());
     *
     * // Already added
     * assert!(mm.add_migration(migration2).is_some());
     * ```
     */
    pub fn add_migration(&mut self, migration: Migration) -> Option<Migration> {
        if self.migrations.contains_key(&migration.id) {
            return Some(migration);
        }

        self.migrations.insert(migration.id, migration);
        None
    }

    /** Bring the database up to date with all the transactions provided.
     */
    pub fn up(&self, connection: &Connection) -> e::Result<()> {
        run_meta_migration(connection)?;

        let applied_migrations = get_applied_migrations(connection)?;

        let mut migrations = self
            .migrations
            .iter()
            .map(|(&id, _)| id)
            .collect::<Vec<_>>();
        migrations.sort_unstable();
        for migration_id in migrations {
            if applied_migrations.contains(&migration_id) {
                continue;
            }

            let migration = &self.migrations[&migration_id];
            if migration.transaction {
                let txn = connection.transaction()?;
                txn.batch_execute(&migration.sql_up)?;
                txn.execute(
                    "
                    INSERT INTO migration_manager(name, reversible) VALUES ($1, $2)
                    ",
                    &[&migration.name, &migration.sql_down.is_some()],
                )?;
                txn.commit()?;
            } else {
                connection.batch_execute(&migration.sql_up)?;
                connection.execute(
                    "
                    INSERT INTO migration_manager(name, reversible) VALUES ($1, $2)
                    ",
                    &[&migration.name, &migration.sql_down.is_some()],
                )?;
            }
        }

        Ok(())
    }

    /** Roll back to the given migration.
     */
    pub fn rollback_to(
        &mut self,
        id: i64,
        connection: &Connection,
    ) -> e::Result<()> {
        // Vector of all the migration IDs
        let applied_migration_ids = get_applied_migrations(connection)?;

        // Check if a migration with that id has been applied
        if !applied_migration_ids.contains(&id) {
            bail!(e::ErrorKind::NoSuchMigration(id));
        }

        let mut migrations_to_undo = applied_migration_ids
            .split(|&mid| mid == id)
            .skip(1)
            .next()
            .unwrap()
            .to_owned();
        migrations_to_undo.reverse(); // put them in newest to oldest order

        let all_reversible = migrations_to_undo
            .iter()
            .all(|mid| self.migrations[mid].sql_down.is_some());
        if !all_reversible {
            bail!(e::ErrorKind::Irreversible);
        }

        for mid in migrations_to_undo {
            revert_migration(connection, &self.migrations[&mid])?;
        }

        Ok(())
    }

    /** Check if the given migration has been added to the `MigrationManager`.
     */
    #[must_use]
    pub fn has_migration(&self, migration: &Migration) -> bool {
        self.migrations.contains_key(&migration.id)
    }
}

pub fn revert_migration(
    connection: &Connection,
    migration: &Migration,
) -> e::Result<()> {
    match &migration.sql_down {
        // If we do have a way of reverting the migration
        Some(sql_down) => {
            let tx = connection.transaction()?;
            tx.batch_execute(sql_down)?;
            tx.execute(
                "DELETE FROM migration_manager WHERE id = $1",
                &[&migration.id],
            )?;
            tx.commit()?;
            Ok(())
        }

        None => Err(e::ErrorKind::NoDownScript(migration.id).into()),
    }
}

fn run_meta_migration(connection: &Connection) -> e::Result<()> {
    const MIGRATION: &str = r"
        CREATE TABLE IF NOT EXISTS migration_manager(
            id BIGSERIAL PRIMARY KEY,
            name TEXT,
            reversible BOOL
        );
    ";

    let tx = connection.transaction()?;
    tx.batch_execute(MIGRATION)?;
    tx.commit()?;

    Ok(())
}

#[must_use]
fn get_applied_migrations(
    connection: &Connection,
) -> postgres::Result<Vec<i64>> {
    let mut db_state = Vec::new();
    let rows = connection.query("SELECT id FROM migration_manager;", &[])?;
    for row in &rows {
        let id = row.get(0);
        db_state.push(id);
    }

    db_state.sort_unstable();

    Ok(db_state)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::File;
    use std::io::Write;
    use utils::*;

    #[test]
    fn test_meta_migration() {
        only_ci!({
            let conn = get_connection();
            assert!(conn.is_active());
            assert!(run_meta_migration(&conn).is_ok());

            // Ensure running it twice is ok
            assert!(run_meta_migration(&conn).is_ok());
        });
    }

    #[test]
    fn test_load_migration() {
        fs::create_dir_all(".tmp/flit_test/0_this-is-a-test").unwrap();

        {
            let mut file =
                File::create(".tmp/flit_test/0_this-is-a-test/up.sql")
                    .expect("Unable to create file");
            file.write_all("Nothing to see here".as_bytes()).unwrap();
            let mut file =
                File::create(".tmp/flit_test/0_this-is-a-test/down.sql")
                    .expect("Unable to create file");
            file.write_all("Nor here".as_bytes()).unwrap();
        }

        let mig = Migration::load_migration(".tmp/flit_test/0_this-is-a-test")
            .unwrap();
        fs::remove_dir_all(".tmp/flit_test").unwrap();

        assert_eq!("Nothing to see here", &mig.sql_up);
        assert_eq!("Nor here", &mig.sql_down.unwrap());
    }
}
